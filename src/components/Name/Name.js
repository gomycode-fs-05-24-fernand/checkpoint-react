import React from 'react';
import './name.css';

const Name = ({name}) => {
    return (
        <div>
            <h3>{name}</h3>
        </div>
    );
}

export default Name;
