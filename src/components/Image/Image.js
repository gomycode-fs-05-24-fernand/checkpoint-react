import React from 'react';

const Image = ({image}) => {
    return (
        <div>
            <img className="m-5 card-img-top" style={{width:'10rem'}} src={image}  alt="..."/>
        </div>
    );
}

export default Image;
