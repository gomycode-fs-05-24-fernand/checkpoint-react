const product = [
  {
    name: "Wireless Mouse",
    price: 15000,
    description: "A wireless mouse with ergonomic design and long battery life.",
    imageUrl: "https://premicecomputer.com/wp-content/uploads/2021/03/Sans-titre-17-1.jpg"
  },
  {
    name: "Raspberry Pi 5",
    price: 75000,
    description: "A wireless mouse with ergonomic design and long battery life.",
    imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQe_FMIvizaCsXxA3OzLwiBR2So8OlgbbUFOQ&s"
  },
  {
    name: "Hisense TV",
    price: 150000,
    description: "A wireless mouse with ergonomic design and long battery life.",
    imageUrl: "https://sociam.ci/gestion/products/bc419942-4e36-4775-a0d7-1eb542a4f4d4.webp"
  },
  {
    name: "Bouquet de Rose",
    price: 30000,
    description: "A wireless mouse with ergonomic design and long battery life.",
    imageUrl: "https://m.media-amazon.com/images/I/51WTkiGRZlL._AC_UF1000,1000_QL80_.jpg"
  },
  {
    name: "Raspberry Pi 4",
    price: 50000,
    description: "A wireless mouse with ergonomic design and long battery life.",
    imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQe_FMIvizaCsXxA3OzLwiBR2So8OlgbbUFOQ&s"
  },
]

export default product;
