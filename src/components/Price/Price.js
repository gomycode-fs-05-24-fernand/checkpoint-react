import React from 'react';

const Price = ({price}) => {
    return (
        <div>
            <p><strong>{price}</strong> XOF</p>
        </div>
    );
}

export default Price;
