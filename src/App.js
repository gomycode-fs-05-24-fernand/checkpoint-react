import './App.css';
import Name from './components/Name/Name';
import Price from './components/Price/Price';
import Description from './components/Description/Description';
import Image from './components/Image/Image';
import Card from 'react-bootstrap/Card';
import Products from './product.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import userPicture from './assets/userProfile.jpeg';

let username = "Fernand";

const App = () => (
  
  <div className="App">
    <div className="p-4 d-flex flex-row gap-4 border-9 flex-wrap justify-content-center">
    {Products.map((product, key) => (
      <Card key={key} style={{ width: '18rem' }}>
        <Image image={product.imageUrl} />
        <Card.Body>
          <Card.Title>
            <Name name={product.name} />
          </Card.Title>
          <Card.Text>
            <Description description={product.description} />
          </Card.Text>
          <Card.Text>
            <Price price={product.price} />
          </Card.Text>
        </Card.Body>
      </Card>
    ))}
    </div>
    <User username={username} />

  </div>
);

export default App;

function User({username}) {
  return <div className="text-center my-5">
  <h1 className="display-4">Hello, {username ?? "There"}</h1>
  {username && (
    <img 
      src={userPicture} 
      alt={`${username}'s profile`} 
      className="rounded-circle mt-4" 
      style={{ width: '150px', height: '150px', objectFit: 'cover' }}
    />
  )}
</div>
}
